/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <init.h>

#include "cats.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(cats_developer, CONFIG_CATS_LOG_LEVEL);

static struct cats_screen scr;

static int register_developer_screen(const struct device *unused)
{
	lv_obj_t *label, *cont;

	LOG_DBG("Creating developer settings screen");

	scr.name = "developer";
	scr.obj = lv_obj_create(NULL);
	scr.orientation = CATS_SCREEN_VERTICAL;

	label = lv_label_create(scr.obj);
	lv_label_set_text(label, "Developer settings");
	lv_obj_align(label, LV_ALIGN_TOP_MID, 0, 0);

	cont = lv_obj_create(scr.obj);
	lv_obj_align(cont, LV_ALIGN_CENTER, 0, 0);
        lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);
	lv_obj_set_size(cont, 220, 280);

	cats_add_screen(&scr);

	return 0;
}
CATS_SCREEN_INIT(register_developer_screen);
